const { barang, pelanggan, pemasok } = require("../../models"); // Import all models
const validator = require("validator");

exports.create = async (req, res, next) => {
    try {
      let error = [];
      if (!validator.isAlpha(validator.blacklist(req.body.nama," "))) {
        error.push("Name must be an Alphabet ");
      }
  
      if (error.length > 0) {
        return res.status(404).json({
          message: error.join(", "),
        });
      }
      next();
    } catch (e) {
      // console.log(e);
  
      return res.status(500).json({
        message: "Internal server error",
        error: e,
      });
    }
  };

  exports.update = async (req, res, next) => {
    try {
      let error = [];
      if (!validator.isAlpha(validator.blacklist(req.body.nama," "))) {
        error.push("Name must be an Alphabet ");
      }
  
      if (error.length > 0) {
        return res.status(404).json({
          message: error.join(", "),
        });
      }
      next();
    } catch (e) {
      console.log(e);
  
      return res.status(500).json({
        message: "Internal server error",
        error: e,
      });
    }
  };
const { barang, pelanggan, pemasok } = require("../../models"); // Import all models
const validator = require("validator");

module.exports.create = async (req, res, next) => {
  try {
    let errors = [];
    if (
      !validator.isAlpha(req.body.nama) &&
      !validator.blacklist(req.body.nama, " ")
    ) {
      errors.push("Nama must be a Alphabet");
    }
    if (
      validator.isEmpty(req.body.nama) &&
      !validator.blacklist(req.body.nama, " ")
    ) {
      errors.push("Nama is Empty!");
    }
    if (!validator.blacklist(req.body.nama, " ")) {
      errors.push("Nama must be a Alphabet");
    }
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // It means that will be go to the next middleware
    next();
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      message: "Internal Server Error",
      error: e,
    });
  }
};

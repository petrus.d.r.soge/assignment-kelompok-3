const { MongoClient } = require("mongodb"); //Import Mongo Client

const uri = process.env.MONGO_URI; //Address Cluster Server

const connection = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}); //make new connection

//run the connection
try {
    connection.connect();
    console.log("MongoDB connect!");
} catch (e) {
    console.error(e);
}

module.exports = connection;

const express = require("express"); // Import express

// Import validator
const pelangganValidator = require("../middlewares/validators/pelangganValidator");

// Import controller
const  pelangganController = require("../controllers/pelangganController");

// Make router
const router = express.Router();

// Get All Data
router.get("/", pelangganController.getAll);

// Create data
router.post("/", pelangganValidator.create, pelangganController.create);

// Get One Data
router.get("/:id", pelangganController.getOne);

// Update Data
router.put("/:id", pelangganValidator.update, pelangganController.update);

// Delete One Data
router.delete("/:id", pelangganController.delete);

// Export router
module.exports = router;

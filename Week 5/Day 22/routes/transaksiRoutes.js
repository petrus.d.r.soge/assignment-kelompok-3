const express = require("express");
const transaksiController = require("../controllers/transaksiController");
const router = express.Router();

router.get("/", transaksiController.getAll);
router.get("/:id", transaksiController.getOne);
router.delete("/:id", transaksiController.delete);

module.exports = router;

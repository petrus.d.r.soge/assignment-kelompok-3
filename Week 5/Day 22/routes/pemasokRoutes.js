const express = require("express");
const pemasokController = require("../controllers/pemasokController");
const pemasokValidator = require("../middlewares/validators/pemasokValidator");
const router = express.Router();

router.get("/", pemasokController.getAll);
router.get("/:id", pemasokController.getOne);
router.post("/", pemasokValidator.create, pemasokController.create);
router.put("/:id", pemasokValidator.create, pemasokController.update);
router.delete("/:id", pemasokController.delete);

module.exports = router;

require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

const express = require("express");

const transaksiRoute = require("./routes/transaksiRoutes");
const pemasokRoute = require("./routes/pemasokRoutes");
const pelangganRoute = require("./routes/pelangganRoutes");
const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use("/transaksi", transaksiRoute);
app.use("/pemasok", pemasokRoute);
app.use("/", pelangganRoute);

app.listen(3000, () => console.log("This server running on 3000"));

const { ObjectId } = require("mongodb");
const connection = require("../models");

class PemasokController {
  //get All Data
  async getAll(req, res) {
    try {
      const dbConnection = connection.db("penjualan_morning"); //connect to database penjualan_morning
      const pemasok = dbConnection.collection("pemasok"); //connect to table/collection transaksi

      let data = await pemasok.find({}).toArray();

      if (data.length === 0) {
        return res.status(404).json({
          message: "Pemasok Not Found",
        });
      }

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async getOne(req, res) {
    try {
      const penjualan = connection.db("penjualan_morning"); // Connect to penjualan database
      const pemasok = penjualan.collection("pemasok"); // Connect to transaksi collection / table

      // Find one data
      let data = await pemasok.findOne({
        _id: new ObjectId(req.params.id),
      });

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async delete(req, res) {
    try {
      const penjualan = connection.db("penjualan_morning"); // Connect to penjualan database
      const pemasok = penjualan.collection("pemasok"); // Connect to transaksi collection / table

      let data = await pemasok.deleteOne({
        _id: new ObjectId(req.params.id),
      });
      return res.status(200).json({
        message: "Success to delete",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async create(req, res) {
    try {
      const penjualan = connection.db("penjualan_morning"); // Connect to penjualan database
      const pemasok = penjualan.collection("pemasok");
      let data = await pemasok.insertOne({
        nama: req.body.nama,
      });
      return res.status(200).json({
        message: "Success to create",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async update(req, res) {
    const penjualan = connection.db("penjualan_morning"); // Connect to penjualan database
    const pemasok = penjualan.collection("pemasok");
    try {
      let updateData = await pemasok.updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: {
            nama: req.body.nama,
          },
        }
      );
      //Find data yg di update
      let data = await pemasok.findOne({
        _id: new ObjectId(req.params.id),
      });
      return res.status(200).json({
        message: "Success to update",
        data,
      });
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}
module.exports = new PemasokController();

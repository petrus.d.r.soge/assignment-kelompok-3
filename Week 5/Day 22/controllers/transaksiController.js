const { ObjectId } = require("mongodb");
const connection = require("../models");

class TransaksiController {
  //get All Data
  async getAll(req, res) {
    try {
      const dbConnection = connection.db("penjualan_morning"); //connect to database penjualan_morning
      const transaksi = dbConnection.collection("transaksi"); //connect to table/collection transaksi

      let data = await transaksi.find({}).toArray();

      if (data.length === 0) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async getOne(req, res) {
    try {
      const penjualan = connection.db("penjualan_morning"); // Connect to penjualan database
      const transaksi = penjualan.collection("transaksi"); // Connect to transaksi collection / table

      // Find one data
      let data = await transaksi.findOne({
        _id: new ObjectId(req.params.id),
      });

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async delete(req, res) {
    try {
      const penjualan = connection.db("penjualan_morning"); // Connect to penjualan database
      const transaksi = penjualan.collection("transaksi"); // Connect to transaksi collection / table

      let data = await transaksi.deleteOne({
        _id: new ObjectId(req.params.id),
      });
      return res.status(200).json({
        message: "Success to delete",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async create(req, res) {
    try {
      const penjualan = connection.db("penjualan_morning"); // Connect to penjualan database
      const transaksi = penjualan.collection("transaksi");
      let data = await transaksi.insertOne({
        barang: req.body.barang,
        pelanggan: req.body.pelanggan,
        jumlah: req.body.jumlah,
        total: req.body.total,
      });
      return res.status(200).json({
        message: "Success to delete",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async update(req, res) {
    try {
      await transaksi.updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: {
            barang: req.body.barang,
            pelanggan: req.body.pelanggan,
            jumlah: req.body.jumlah,
            total: req.body.total,
          },
        }
      );
      //Find data yg di update
      let data = await transaksi.findOne({
        _id: new ObjectId(req.params.id),
      });
      return res.status(200).json({
        message: "Success to delete",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}
module.exports = new TransaksiController();

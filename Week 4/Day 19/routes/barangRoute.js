const express = require("express"); //import express
const router = express.Router(); // Make a router

//Import controller
const barangController = require("../controllers/barangController");

router.get("/", barangController.getAll);
router.post("/", barangController.create);
router.get("/:id", barangController.getOne);
router.put("/:id", barangController.update);
router.delete("/:id", barangController.deleteData);

module.exports = router; //Export router

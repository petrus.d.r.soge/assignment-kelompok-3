const express = require("express");
const router = express.Router();

const transaksiController = require("../controllers/transaksiController");

router.get("/", transaksiController.getAll);
router.get("/:id", transaksiController.getOne);
router.post("/", transaksiController.create);
router.put("/:id", transaksiController.updateData);
router.delete("/:id", transaksiController.deleteData);

module.exports = router;

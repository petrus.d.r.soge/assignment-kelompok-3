const express = require("express");
const router = express.Router();

// Import Controller
const pelangganController = require("../controllers/pelangganController");

router.get("/", pelangganController.getAll);
router.get("/:id", pelangganController.getOne);
router.post("/", pelangganController.create);
router.put("/:id", pelangganController.updateData);
router.delete("/:id", pelangganController.deleteData);
module.exports = router;

//import mysql connection
const mysql = require("../models");

const getAll = (req, res) => {
  let sql =
    "SELECT t.id ,b.nama AS nama_barang,p.nama AS nama_pelanggan,p2.nama AS nama_pemasok,b.harga ,t.waktu ,t.jumlah ,t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON p.id = t.id_pelanggan JOIN pemasok p2 ON b.id_pemasok = p2.id ORDER BY t.id;";

  mysql.query(sql, (err, results) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    return res.status(200).json({
      message: "Success",
      data: results,
    });
  });
};

const create = (req, res) => {
  //Find Barang
  let findSqlBarang = "SELECT * FROM barang WHERE id = ?";
  mysql.query(findSqlBarang, [req.body.id_barang], (err, results) => {
    let price = eval(results[0].harga);
    let total = eval(req.body.jumlah * price);
    // console.log(total);

    //create SQL insert into
    let createSql =
      "INSERT INTO transaksi(id_barang, id_pelanggan, jumlah, total) VALUES (?, ?, ?,?);";

    mysql.query(
      createSql,
      [req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, total],
      (err, results) => {
        if (err) {
          return res.status(500).json({
            message: "Internal Server Error",
            error: err,
          });
        }
        //if Success
        let sqlSelect = `SELECT t.id ,b.nama AS nama_barang,p.nama AS nama_pelanggan,p2.nama AS nama_pemasok,b.harga ,t.waktu ,t.jumlah ,t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON p.id = t.id_pelanggan JOIN pemasok p2 ON b.id_pemasok = p2.id WHERE t.id = ${results.insertId};`;

        mysql.query(sqlSelect, (err, results) => {
          if (err) {
            return res.status(500).json({
              message: "Internal Server Error",
              error: err,
            });
          }
          return res.status(201).json({
            message: "Success",
            data: results[0],
          });
        });
      }
    );
  });
};

const updateData = (req, res) => {
  let findSqlBarang = "SELECT * FROM barang WHERE id = ?";
  mysql.query(findSqlBarang, [req.body.id_barang], (err, results) => {
    let price = eval(results[0].harga);
    let total = eval(req.body.jumlah * price);

    let sqlUpdate =
      "UPDATE transaksi SET id_barang = ?, id_pelanggan = ?, jumlah = ?,total = ? WHERE id = ?";
    mysql.query(
      sqlUpdate,
      [
        req.body.id_barang,
        req.body.id_pelanggan,
        req.body.jumlah,
        total,
        req.params.id,
      ],
      (err, results) => {
        if (err) {
          return res.status(500).json({
            message: "Internal Server Error",
            error: err,
          });
        }
        let sqlGetOne =
          " SELECT t.id ,b.nama AS nama_barang,p.nama AS nama_pelanggan,p2.nama AS nama_pemasok,b.harga ,t.waktu ,t.jumlah ,t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON p.id = t.id_pelanggan JOIN pemasok p2 ON b.id_pemasok = p2.id WHERE t.id = ?;";
        mysql.query(sqlGetOne, [req.params.id], (err, results) => {
          if (err) {
            return res.status(500).json({
              message: "Internal Server Error",
              error: err,
            });
          }
          return res.status(200).json({
            message: "Success",
            data: results[0],
          });
        });
      }
    );
  });
};

const deleteData = (req, res) => {
  let sqlDelete = "DELETE FROM transaksi WHERE id = ?";

  mysql.query(sqlDelete, [req.params.id], (err, results) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    return res.status(200).json({
      message: "Success",
      data: results[0],
    });
  });
};

const getOne = (req, res) => {
  let sqlGetOne =
    " SELECT t.id ,b.nama AS nama_barang,p.nama AS nama_pelanggan,p2.nama AS nama_pemasok,b.harga ,t.waktu ,t.jumlah ,t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON p.id = t.id_pelanggan JOIN pemasok p2 ON b.id_pemasok = p2.id WHERE t.id = ?;";
  mysql.query(sqlGetOne, [req.params.id], (err, results) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    return res.status(200).json({
      message: "Success",
      data: results[0],
    });
  });
};

module.exports = { getAll, create, deleteData, updateData, getOne };

//import mysql connection
const mysql = require("../models");

const getAll = (req, res) => {
  let sql = "SELECT * FROM pemasok ORDER BY id";

  mysql.query(sql, (err, results) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    return res.status(200).json({
      message: "Success",
      data: results,
    });
  });
};

const create = (req, res) => {
  let createPemasok = "INSERT INTO pemasok (nama) VALUES(?)";
  mysql.query(createPemasok, [req.body.nama], (err, results) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    let sqlSelect = `SELECT * FROM pemasok WHERE id = ${results.insertId}`;
    mysql.query(sqlSelect, (err, results) => {
      if (err) {
        return res.status(500).json({
          message: "Internal Server Error",
          error: err,
        });
      }
      return res.status(201).json({
        message: "Success",
        data: results[0],
      });
    });
  });
};

const updateData = (req, res) => {
  let updatePemasok = "UPDATE pemasok SET nama = ? WHERE id = ?";
  mysql.query(updatePemasok, [req.body.nama, req.params.id], (err, results) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    let sqlSelect = `SELECT * FROM pemasok WHERE id = ?`;
    mysql.query(sqlSelect, [req.params.id], (err, results) => {
      if (err) {
        return res.status(500).json({
          message: "Internal Server Error",
          error: err,
        });
      }
      return res.status(201).json({
        message: "Success",
        data: results[0],
      });
    });
  });
};

const deleteData = (req, res) => {
  let sqlDelete = "DELETE FROM pemasok WHERE id = ?";

  mysql.query(sqlDelete, [req.params.id], (err, results) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    return res.status(200).json({
      message: "Success",
      data: results[0],
    });
  });
};

const getOne = (req, res) => {
  let sqlGetOne = "SELECT * FROM pemasok WHERE id = ?";
  mysql.query(sqlGetOne, [req.params.id], (err, results) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    return res.status(200).json({
      message: "Success",
      data: results[0],
    });
  });
};

module.exports = { getAll, create, deleteData, updateData, getOne };

// Import mysql connection
// const { query } = require("../models/connection");
const connection = require("../models");

// GETALL data from barang
const getAll = (req, res) => {
  // Make a sql query
  let getAllBarang = "SELECT * FROM barang";

  // Run the sql query
  connection.query(getAllBarang, (err, results) => {
    // If error, it will go to here
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    // If no error, it will go here
    return res.status(200).json({
      message: "Success",
      data: results,
    });
  });
};
// GETONE from barang
const getOne = (req, res) => {
  let getOneBarang = "SELECT * FROM barang WHERE id = ?";
  connection.query(getOneBarang, [req.params.id], (err, results) => {
    // If error, it will go to here
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    // If no error, it will go here
    return res.status(200).json({
      message: "Success",
      data: results[0],
    });
  });
};
//CREATE data barang
const create = (req, res) => {
  // Make sql query for barang
  let createBarang = "INSERT INTO barang VALUES (?, ?, ?, ?)";

  // Run Query Create
  connection.query(
    createBarang,
    [
      req.body.id_barang,
      req.body.nama_barang,
      req.body.harga,
      req.body.id_pemasok,
    ],
    (err, results) => {
      // If error
      if (err) {
        return res.status(500).json({
          message: "Internal Server Error",
          error: err,
        });
      }
      // If Success
      let selectBarang = `SELECT * FROM barang WHERE id = ${results.insertId}`;
      // Run select query
      connection.query(selectBarang, (err, results) => {
        // if error
        if (err) {
          return res.status(500).json({
            message: "Internal Server Error",
            error: err,
          });
        }
        // If success
        return res.status(201).json({
          message: "Barang Berhasil Ditambahkan",
          data: results[0],
        });
      });
    }
  );
};
//DELETE data from barang
const deleteData = (req, res) => {
  let deleteBarang = "DELETE FROM barang WHERE id = ?";

  connection.query(deleteBarang, [req.params.id], (err, results) => {
    // if error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
    // If success
    return res.status(200).json({
      message: "Successfully Deleted",
    });
  });
};
//UPDATE data from barang
const update = (req, res) => {
  let updateBarang = "UPDATE barang SET id_barang = ?, harga = ? WHERE id = ?";
  connection.query(
    updateBarang, [req.body.id_barang, req.body.harga, req.params.id], (err, results) => {
      let selectBarang = `SELECT * FROM barang WHERE id = ?`;
  connection.query(selectBarang, [req.params.id], (err, results) => {
        if (err) {
          return res.status(500).json({
            message: "Internal Server Error",
            error: err,
          });
        }
        return res.status(201).json({
          message: "Success",
          data: results[0],
        });
      });
    }
  );
};

module.exports = { getAll, getOne, create, update, deleteData }; // Export All functions

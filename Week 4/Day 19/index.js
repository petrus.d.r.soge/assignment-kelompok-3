const express = require("express");
const app = express();
const transaksiRoutes = require("./routes/transaksiRoutes");
const barangRoute = require("./routes/barangRoute");
const pemasokRoutes = require("./routes/pemasokRoutes");
const pelangganRoutes = require("./routes/pelangganRoute");


//read req.body
app.use(express.urlencoded({ extended: true }));

//define route
app.use("/transaksi", transaksiRoutes);
app.use("/barang", barangRoute);
app.use("/pemasok", pemasokRoutes);
app.use("/pelanggan", pelangganRoutes);

//server running
app.listen(3000, () => console.log("This server is running on 3000"));

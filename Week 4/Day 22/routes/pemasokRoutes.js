const express = require("express");
const router = express.Router();

const pemasokValidator = require("../middlewares/validators/pemasokValidator");

const pemasokController = require("../controllers/pemasokController");

router.post("/", pemasokValidator.create, pemasokController.create);
router.get("/", pemasokController.getAll);
router.get("/:id", pemasokController.getOne);
router.put("/:id", pemasokValidator.create, pemasokController.update);
router.delete("/:id", pemasokController.delete);

module.exports = router;

const express = require("express");
const router = express.Router();

// Import Controller
const PelangganController = require("../controllers/pelangganController");
// Import Validator
const PelangganValidator = require("../middlewares/validators/pelangganValidator")
// Connting Routes to Port
router.get("/", PelangganController.getAll);
router.get("/:id", PelangganController.getOne);
router.post("/", PelangganValidator.create,PelangganController.create);
router.put("/:id",PelangganValidator.update, PelangganController.update);
router.delete("/:id", PelangganController.delete);

module.exports = router;

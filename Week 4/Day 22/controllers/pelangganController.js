const e = require("express");
const { pelanggan, pemasok, barang, transaksi } = require("../models");

class PelangganController {
  async getAll(req, res) {
    try {
      let data = await pelanggan.findAll({
        // attributes:[id, nama, [createAt, waktu]]
      });
      // console.log(data);
      return res.status(200).json({
        message: "success",
        data: data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "success",
        error: e,
      });
    }
  }

  async getOne(req, res) {
    try {
      let data = await pelanggan.findOne({
        where: { id: req.params.id },
        attributes: ["id", "nama", ["createdAt", "waktu"]],
      });

      return res.status(200).json({
        message: "success",
        data: data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "success",
        error: e,
      });
    }
  }
  async create(req, res) {
    try {
      let createData =await pelanggan.create({
        nama: req.body.nama,
      });

      let data = await pelanggan.findOne({
        where: { id: createData.id },
      });

      return res.status(200).json({
        message: "success",
        data: data,
      });
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "interval server error!",
      });
    }
  }

  async update(req, res) {
    try {
      let update = await {
        nama: req.body.nama,
      }

      let updateData = await pelanggan.update(update,{
        where:{id: req.params.id}
      })
      let data = await pelanggan.findOne({
        where: { id: req.params.id },
      });
      return res.status(200).json({
        message: "success",
        data: data,
      });

    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "interval server error!",
      });
    }
  }

  async delete(req, res) {
    try {
      let data = await pelanggan.destroy({
        where:{id: req.params.id}
      })

      return res.status(202).json({
        message: "data deleted success"
      })
    } catch (e) {
      // console.log(e);
      return res.status(500).json({
        message: "interval server error!",
      });
    }
  }
}
module.exports = new PelangganController();

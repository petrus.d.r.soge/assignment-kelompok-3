const { barang, pelanggan, pemasok } = require("../models");

class PemasokController {
  async getAll(req, res) {
    try {
      let data = await pemasok.findAll({
        attributes: ["id", "nama", ["createdAt", "Waktu"]],
      });
      if (data.length === 0) {
        return res.status(404).json({
          message: "Pemasok Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async getOne(req, res) {
    try {
      let data = await pemasok.findOne({
        where: { id: req.params.id },
        attributes: ["id", "nama", ["createdAt", "waktu"]],
      });
      return res.status(200).json({
        message: "Success",
        data: data,
      });
    } catch (e) {
      console.log(e);
      return res.status(404).json({
        message: "Pemasok Not Found",
      });
    }
  }
  async create(req, res) {
    try {
      // Will create data
      let createdData = await pemasok.create({
        nama: req.body.nama,
      });

      let data = await pemasok.findOne({
        where: { id: createdData.id },
        attributes: ["id", "nama", ["createdAt", "waktu"]], // just these attributes that showe
      });

      // If success
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If error
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async update(req, res) {
    try {
      let update = {
        nama: req.body.nama,
      };
      let updatedData = await pemasok.update(update, {
        where: {
          id: req.params.id,
        },
      });

      let data = await pemasok.findOne({
        where: { id: req.params.id },
        attributes: ["id", "nama", ["createdAt", "waktu"]], //
      });

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async delete(req, res) {
    try {
      let dataFind = await pemasok.findOne({
        where: { id: req.params.id },
        attributes: ["id", "nama", ["createdAt", "waktu"]], //
      });
      let data = await pemasok.destroy({ where: { id: req.params.id } });

      if (!data) {
        return res.status(404).json({
          message: "Pemasok Not Found",
        });
      }

      return res.status(200).json({
        message: "Success delete pemasok",
        dataFind,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new PemasokController();

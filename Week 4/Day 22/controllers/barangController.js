const { barang, pelanggan, pemasok } = require("../models");

class BarangController {
  async create(req, res) {
    try {
      let createdData = await barang.create({
        nama: req.body.nama,
        harga: req.body.harga,
        id_pemasok: req.body.id_pemasok,
        image: req.body.image ? req.body.image : null,
      });

      let data = await barang.findOne({
        where: {
          id: createdData.id,
        },
        attributes: ["id", "harga", "image", "createdAt", "updatedAt"],
        include: [{ model: pemasok, attributes: ["nama"] }],
      });

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async getAll(req, res) {
    try {
      let data = await barang.findAll({
        attributes: ["id", "nama", "harga", "id_pemasok"],
        include: [
          {
            model: pemasok,
            attributes: ["nama"],
          },
        ],
      });
      if (data.length === 0) {
        return res.status(404).json({
          message: "Barang Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async getOne(req, res) {
    try {
      let data = await barang.findOne({
        where: { id: req.params.id },
        attributes: ["id", "nama", "harga", "id_pemasok"],
        include: [
          {
            model: pemasok,
            attributes: ["nama"],
          },
        ],
      });
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async update(req, res) {
    let update = {
      nama: req.body.nama,
      harga: req.body.harga,
      id_pemasok: req.body.id_pemasok,
      image: req.body.image ? req.body.image : null,
    };
    try {
      let updatedData = await barang.update(update, {
        where: {
          id: req.params.id,
        },
      });
      let data = await barang.findOne({
        where: { id: req.params.id },
        attributes: ["id", "nama", "harga", "id_pemasok"],
        include: [
          {
            model: pemasok,
            attributes: ["nama"],
          },
        ],
      });
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new BarangController();

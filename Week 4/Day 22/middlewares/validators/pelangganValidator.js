const { pemasok, pelanggan, barang, transaksi } = require("../../models");
const validator = require("validator");


exports.create = async (req, res, next) => {
  try {
    let error = [];
    if (!validator.isAlpha(validator.blacklist(req.body.nama," "))) {
      error.push("name is contain none alphabet ");
    }

    if (error.length > 0) {
      return res.status(404).json({
        message: error.join(", "),
      });
    }
    next();
  } catch (e) {
    // console.log(e);

    return res.status(500).json({
      message: "Internal server error",
      error: e,
    });
  }
};

exports.update = async (req,res,next)=>{
    try {
        let error = [];
        if (!validator.isAlpha(validator.blacklist(req.body.nama," "))) {
          error.push("name is contain none alphabet ");
        }
    
        console.log(req.body.nama)
        if (error.length > 0) {
          return res.status(404).json({
            message: error.join(", "),
          });
        }
        next();
      } catch (e) {
        console.log(e);
    
        return res.status(500).json({
          message: "Internal server error",
          error: e,
        });
      }

}